import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from "@angular/router";
import {CustomerMessageThreadListComponent} from "./customer-message-thread-list/customer-message-thread-list.component";
import {CustomerMessageThreadDetailsComponent} from "./customer-message-thread-details/customer-message-thread-details.component";
import {CustomerMessageListComponent} from "./customer-message-list/customer-message-list.component";
import {DeepLinkComponent} from "./deep-link/deep-link.component";
import {AppComponent} from "./app.component";
import {CustomerMessageDetailsComponent} from "./customer-message-details/customer-message-details.component";

const APP_ROUTES: Routes = [

  {
    // Route 1
    path: 'threads',
    component: CustomerMessageThreadListComponent,

    children: [
      {
        // Route 1.1
        path: 'details',
        component: CustomerMessageThreadDetailsComponent,

        children: [
          {
            // Route 1.1.1
            path: ':id',
            component: CustomerMessageThreadDetailsComponent,
          }
        ],
      },
      {
        // Route 1.2
        path: '',
        redirectTo: 'details',
        pathMatch: 'full',
      },
      {
        // Route 1.3
        path: ':tid',

        children: [
          {
            // Route 1.3.1
            path: 'messages',
            component: CustomerMessageListComponent,

            children: [
              {
                // Route 1.3.1.1
                path: '',
                redirectTo: 'details',
                pathMatch: 'full',
              },
              {
                // Route 1.3.1.2
                path: 'details',
                component: CustomerMessageDetailsComponent,

                children: [
                  {
                    path: ':mid',
                    component: CustomerMessageDetailsComponent
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  {
    // Route 2
    path: 'common',

    children: [
      {
        path: 'deep-link',
        component: DeepLinkComponent,
      }
    ]
  },
  {
    // Route 3
    path: '**',
    component: AppComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(APP_ROUTES),
  ],
  exports: [
    RouterModule,
  ],
  declarations: []
})
export class AppRoutingModule {
}
