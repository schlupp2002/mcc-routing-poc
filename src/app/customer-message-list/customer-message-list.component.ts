import {Component, OnInit} from '@angular/core';
import {Params, ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-customer-message-list',
  templateUrl: './customer-message-list.component.html',
  styleUrls: ['./customer-message-list.component.css']
})
export class CustomerMessageListComponent implements OnInit {


  private paramSubscription: Subscription;
  private threadId: number;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {

    this.paramSubscription =
      this.route.params.subscribe((params: Params) => {

        this.threadId = +params['tid'];
      });
  }




}
