import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-customer-message-details',
  templateUrl: './customer-message-details.component.html',
  styleUrls: ['./customer-message-details.component.css']
})
export class CustomerMessageDetailsComponent implements OnInit, OnDestroy {

  messageId: number = 0;

  private paramSubscription: Subscription;


  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.paramSubscription =
      this.route.params.subscribe((params: Params) => {

        this.messageId = +params['mid'];
      });
  }


  ngOnDestroy(): void {

    this.paramSubscription.unsubscribe();
  }
}
