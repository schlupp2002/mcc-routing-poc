import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";

import 'rxjs/add/operator/switchMap';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-customer-message-thread-details',
  templateUrl: './customer-message-thread-details.component.html',
  styleUrls: ['./customer-message-thread-details.component.css']
})
export class CustomerMessageThreadDetailsComponent implements OnInit, OnDestroy {

  threadId: number;
  idSubscription: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.idSubscription = this.route.params.subscribe((params: Params) => this.threadId = params['id']);
  }


  ngOnDestroy(): void {

    this.idSubscription.unsubscribe();
  }
}
