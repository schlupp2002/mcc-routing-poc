import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CustomerMessageThreadListComponent } from './customer-message-thread-list/customer-message-thread-list.component';
import { CustomerMessageThreadDetailsComponent } from './customer-message-thread-details/customer-message-thread-details.component';
import { CustomerMessageListComponent } from './customer-message-list/customer-message-list.component';
import { CustomerMessageDetailsComponent } from './customer-message-details/customer-message-details.component';
import { DeepLinkComponent } from './deep-link/deep-link.component';

import {AppRoutingModule} from "./app-routing.module";

@NgModule({
  declarations: [
    AppComponent,
    CustomerMessageThreadListComponent,
    CustomerMessageThreadDetailsComponent,
    CustomerMessageListComponent,
    CustomerMessageDetailsComponent,
    DeepLinkComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
