import { MccRoutingPocPage } from './app.po';

describe('mcc-routing-poc App', function() {
  let page: MccRoutingPocPage;

  beforeEach(() => {
    page = new MccRoutingPocPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
